<?php

require_once('labbook.php');

class ListingCSharp extends Listing {
	
	/* MonoDevelop */
	/*function __construct() {

		parent::__construct("sharpcode");

		$keywords = new KeywordSet("csharpkeyword");
		$keywords->append(array("using", "public", "private", "protected", "class", "new", "if", "else",
								"static", "return", "this", "switch", "case", "break", "continue","foreach","for"));
		$this->registerKeywordSet($keywords);

		$types = new KeywordSet("csharptype");
		$types->append(array("MonoBehaviour", "int", "float", "double", "void", "bool", "string", "Mathf", "Vector3",
								"Vector2", "Time", "KeyCode", "Input", "Collider2D", "Collision2D", "Transform", "Random",
								"Renderer", "Camera", "Plane", "GeometryUtility","Screen", "AudioClip","AudioSource",
								"GUI", "Rect", "Color", "TextAnchor", "FontStyle", "Application", "Component", "null",
								"GameObject","Text","Slider", "Sprite", "SpriteRenderer", "false", "true", "Animator",
								"EdgeCollider2D"));
		$this->registerKeywordSet($types);

	*/	
	//	$commentsBlock = new BlockSet("/*","*/","csharpcomment");
	/*	$this->registerBlockSet($commentsBlock);
		
		$commentsLine = new LineSet("//", "csharpcomment");
		$this->registerLineSet($commentsLine);

		$stringBlock = new BlockSet("\"","\"", "csharpstring");
		$this->registerBlockSet($stringBlock);	

		$this->registerBlockSet(new NumberSet("csharpnumber"));
	
		$defineSet = new DefineSet("csharptype", array("class"));
		
		$this->registerDefineSet($defineSet);

		
	}*/

	function __construct() {

		parent::__construct("sharpcode");

		$keywords = new KeywordSet("csharpkeyword1");
		$keywords->append(array("using", "public", "private", "protected", "class", "new", "static", "this", "true", "false"));
		$this->registerKeywordSet($keywords);

		$keywords = new KeywordSet("csharpkeyword2");
		$keywords->append(array("if", "else","return", "this", "switch", "case", "break", "continue","foreach","for","RequireComponent","typeof","ExecuteInEditMode","int", "float", "double", "void", "bool", "string"));
		$this->registerKeywordSet($keywords);


		$types = new KeywordSet("csharptype");
		$types->append(array("MonoBehaviour", "Collider2D", "Collision2D", "Transform", "Random",
								"Renderer", "Camera", "Plane", "GeometryUtility","Screen", "AudioClip","AudioSource",
								"GUI", "Rect", "Color", "TextAnchor", "FontStyle", "Application", "Component", "null",
								"GameObject","Text","Slider", "Sprite", "SpriteRenderer", "false", "true", "Animator",
								"EdgeCollider2D","Vector2","Vector3","LineRenderer","Rigidbody2D", "Time","Input","KeyCode"));
		$this->registerKeywordSet($types);

		
		$commentsBlock = new BlockSet("/*","*/","csharpcomment");
		$this->registerBlockSet($commentsBlock);
		
		$commentsLine = new LineSet("//", "csharpcomment");
		$this->registerLineSet($commentsLine);

		$stringBlock = new BlockSet("\"","\"", "csharpstring");
		$this->registerBlockSet($stringBlock);	

		#$functionBlock = new BlockSet($types,"(", "csharpfunction",false);
		#$this->registerBlockSet($functionBlock);	

		$this->registerBlockSet(new NumberSet("csharpnumber"));
	
		$defineSet = new DefineSet("csharptype", array("class"), " ", true);
		
		$this->registerDefineSet($defineSet);

		
	}

}


?>

