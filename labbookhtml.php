<?php

require_once('labbook.php');
require_once('labbookphp.php');

function topen($str = '') {
	return "&lt;" . $str;
}

function tclose($str = '') {
	return $str . "&gt;";
}

function tag($str) {
	return "&lt;" . $str . "&gt;";
}

class ListingHTMLPHP extends Listing {
	function __construct() {
		$phpBlock = new BlockSet(topen("?php"),tclose("?"), "codephp");
		$phpBlock->parser = new ListingPhp();
		$this->registerBlockSet($phpBlock);	
	}
}
	
function registerTags($lst, $tags, $cssClass) {
	foreach($tags as $tag) {
		$phpParser = new ListingPHP();
		$phpBlock = new BlockSet(topen("?php"),tclose("?"), "codephp");
		$tagBlock = new BlockSet(topen($tag),tclose(), $cssClass);
		$tagBlock->registerInternalParser($phpParser, $phpBlock);
		$lst->registerBlockSet($tagBlock);
		$tagBlock = new BlockSet(topen("/" . $tag),tclose(), $cssClass);		
		$tagBlock->registerInternalParser($phpParser, $phpBlock);
		$lst->registerBlockSet($tagBlock);
	}

}

class ListingHTML extends Listing {
	
	function __construct() {

		parent::__construct("codehtml");

		$commentsBlock = new BlockSet("<!--","-->","htmlcomment");
		$this->registerBlockSet($commentsBlock);
		
		$stringBlock = new BlockSet("\"","\"", "htmlstring");
		$this->registerBlockSet($stringBlock);	

		$phpBlock = new BlockSet(topen("?php"),tclose("?"), "codephp");
		$phpBlock->registerInternalParser(new ListingPhp());
		$this->registerBlockSet($phpBlock);	
		
		$unknownBlock = new BlockSet(topen("?"),tclose("?"), "htmlunknown");
		$this->registerBlockSet($unknownBlock);	
		
		$tag1 = new BlockSet(topen("!"), tclose(), "htmlkeyword");
		$this->registerBlockSet($tag1);	

		$tag2 = new BlockSet([topen("h"), new DigitMarker()], tclose(), "htmltagstyle");
		$this->registerBlockSet($tag2);	

		$tag2 = new BlockSet([topen("/h"), new DigitMarker()], tclose(), "htmltagstyle");
		$this->registerBlockSet($tag2);	

		registerTags($this,['link'],"htmltaghead");
		registerTags($this,['p','strong'],"htmltagstyle");
		registerTags($this,['img'],"htmltagimages");
		registerTags($this,['ul','ol','li'],"htmltaglists");
		registerTags($this,['a'],"htmltaganchors");


		$tag3 = new BlockSet(topen(), tclose(), "htmltaghead");
		$this->registerBlockSet($tag3);	

		
	}


}


?>

