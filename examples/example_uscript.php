<?php
require('../labbookuscript.php');
$lst = new ListingUScript();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Labbook Unity Script test</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="../labbookuscript.css">
</head>
<body>
	<h1>Hello</h1>

	<?php $lst->showFile("example_code.js");?>

</body>
</html>
