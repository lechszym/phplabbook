<?php
require('../labbookswift.php');
$lst = new ListingSwift();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Example labbook swift</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="../labbookswift.css">
</head>
<body>
	<?php $lst->showFile("self.swift");?>


	<?php $lst->showFile("Fraction_02.swift");?>


	<h1>Entire file</h1>

	<?php $lst->showFile("example_code.swift");?>

	<h1>Parts of file hidden</h1>
	
	<?php $lst->showFile("example_code.swift", ['hide',11, 39, 90, 95], ['new', 53, 54]);?>


	<h1>Parts of file shown</h1>
	
	<?php $lst->showFile("example_code.swift", ['show',11, 39, 87, 95]);?>

	<h1>Old code and new code</h1>
	
	<?php $lst->showFile("example_code.swift", ['hide', 1, 10]);?>


</body>
</html>
