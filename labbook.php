<?php

function codeBlock_compare($a, $b) {
	if(!is_a($a,'CodeBlock') || !is_a($a,'CodeBlock')) {
		return 0;
	}


	if($a->startLine < $b->startLine) {
		return -1;
	} else if($a->startLine > $b->startLine) {
		return 1;
	} else {
		if(is_bool($a->opt)) {
			if(is_bool($b->opt)) {
				return 0;
			} else {
				return -1;
			}			
		} else if(is_bool($b->opt)) {
			return 1;
		}
	}
	return 0;

}

class CodeBlock {
	public $startLine;
	public $endLine;
	
	public $opt;
	
	function __construct($startLine, $endLine, $opt = true) {
		$this->startLine = $startLine;
		$this->endLine = $endLine;		
		
		$this->opt = $opt;
	}
}

class ShowBlock extends CodeBlock {
	
	function __construct($startLine, $endLine) {
		parent::__construct($startLine, $endLine, true);
	}
}

class HideBlock extends CodeBlock {
	
	function __construct($startLine, $endLine) {
		parent::__construct($startLine, $endLine, false);
	}
}

class Marker {
	
	private $exceptions;
	public  $M = 1;
	
	function __construct($exceptions = array()) {
		if(!is_array($exceptions)) {
			$this->exceptions = array();
			array_push($this->exceptions,$exceptions);		
		} else {
			$this->exceptions = $exceptions;
		}
	}
	
	function addException($e) {
		array_push($this->exceptions,$e);	
	}
	
	function detect($line) {
		
		foreach($this->exceptions as $e) {
			if(is_string($e)) {
				if(strcmp($e,$line)==0) {
					return false;
				}
			} else if(is_a($e,'Marker')) {
				return $e->detect($line);
			}
		}
		return true;
	}
}

class StringMarker extends Marker {

	public $markerStr;

	function __construct($markerStr) {
		parent::__construct();
		$this->markerStr = $markerStr;
		$this->M = strlen($markerStr);
	}

	function detect($str) {
		if( strcmp($this->markerStr,substr($str,0,$this->M))===0) {
			return true;
		} else {
			return false;
		}
	}

}

class nonAlphaNumericMarker extends Marker {

	function __construct($exceptions = array()) {
		parent::__construct($exceptions);
	}
	
	function detect($line) {
		if(!ctype_alnum($line[0])) {
			return parent::detect($line[0]);		
		} else {
			return false;
		}
	}
}

class DigitMarker extends Marker {

	function __construct($exceptions = array()) {
		parent::__construct($exceptions);
	}
	
	function detect($line) {
		if(ctype_digit($line[0])) {
			return true;		
		} else {
			return !parent::detect($line[0]);
		}
	}
}

class BracketMarker extends Marker {
	private $bracketOpen;
	private $bracektClose;
	private $bracketCount;

	function __construct($bracketType = "()") {
		parent::__construct();
		$this->bracketOpen = substr($bracketType,0,1);
		$this->bracketClose = substr($bracketType,1,1);
		$this->bracketCount = 0;
	}

	function detect($line) {
		if($line[0] == $this->bracketOpen) {
			$this->bracketCount++;
			return false;
		} else if($line[0] == $this->bracketClose) {
			if($this->bracketCount==0) {
				return true;
			} else {
				$this->bracketCount--;
				return false;
			}
		}
		
	}
}

class EndLineMarker extends StringMarker {

	function __construct() {
		parent::__construct('\n');
	}
}

class LineSet {
	public $start;
	public $cssClass;
	public $N;

	function __construct($startSymbol, $cssClass) {
		$this->start = $startSymbol;
		$this->N = strlen($startSymbol);
		$this->cssClass = $cssClass;
	}
}


class BlockSet {

	protected $start = array();
	protected $end = array();
	public $cssClass;
	
	public $N = 0;
	public $M = 0;
	public $inclusiveStart = true;
	public $inclusiveEnd = true;
	public $replaceStart = NULL;
	public $replaceEnd = NULL;

	public $internalParser = array();
	public $internalParserBlock = array();
	public $insideParser = NULL;
	public $keywordCheck = true;

	function __construct($startSymbol, $endSymbol, $cssClass, $inclusiveStart = true, $inclusiveEnd = NULL, $replaceStart = NULL, $replaceEnd = NULL) {
		if(!is_array($startSymbol)) {
			if(is_string($startSymbol)) {
				$startSymbol = new StringMarker($startSymbol);
			}
			array_push($this->start,$startSymbol);
		} else {
			foreach($startSymbol as $start) {
				if(is_string($start)) {
					$start = new StringMarker($start);
				}
				array_push($this->start,$start);			
			}
		}

		if(!is_array($endSymbol)) {
			if(is_string($endSymbol)) {
				$endSymbol = new StringMarker($endSymbol);
			}
			array_push($this->end,$endSymbol);
		} else {
			foreach($endSymbol as $end) {
				if(is_string($end)) {
					$end = new StringMarker($end);
				}
				array_push($this->end,$end);			
			}
		}
		 
		foreach($this->start as $start) {
			$this->N += $start->M;
		}

		foreach($this->end as $end) {
			$this->M += $end->M;
		}
		
		$this->cssClass = $cssClass;
		$this->inclusiveStart = $inclusiveStart;
		if(is_null($inclusiveEnd)) {
			$this->inclusiveEnd = $inclusiveStart;
		} else {
			$this->inclusiveEnd = $inclusiveEnd;		
		}
		$this->replaceStart = $replaceStart;
		$this->replaceEnd = $replaceEnd;
	}
	
	function check($symbols, $str) {
		$M = 0;
		foreach($symbols as $marker) {
			if(!$marker->detect(substr($str,$M,$marker->M))) {
				return false;
			}
			$M += $marker->M;
		}
		return true;	
	}
	
	
	function checkStart($str) {
		return $this->check($this->start,$str);
	}
	
	function checkEnd($str) {
		return $this->check($this->end,$str);
	}
	
	function registerInternalParser($parser, $block = NULL) {
		array_push($this->internalParser,$parser);
		array_push($this->internalParserBlock,$block);
	}
	
}

class NumberSet extends BlockSet {

	function __construct($cssClass) {
		parent::__construct(new DigitMarker(), new nonAlphaNumericMarker(), $cssClass, true, false);	
		$this->keywordCheck = false;
	}
}

class KeywordSet {
	public $keywords = array();
	public $cssClass;
	public $mustFollow = "";
	
	function __construct($cssClass, $mustFollow = "") {
		$this->cssClass = $cssClass;
		$this->mustFollow = $mustFollow;
	}
	
	function append($keywords) {
		if(is_array($keywords)) {
			foreach ($keywords as $k) {
			    array_push($this->keywords,$k);
			}
		} else {
			array_push($this->keywords,$keywords);
		}				
	}

}

class DefineSet extends KeywordSet {

	public $predefKeywords;
	public $token;
	public $retro;

	function __construct($cssClass, $predefKeywordSet, $mustFollow = "", $retro = false) {
		parent::__construct($cssClass, $mustFollow);
		$this->predefKeywords = $predefKeywordSet;
		$this->retro = $retro;

	}
}

class Listing {
	
	private	  $keywordSets = array();
	private   $blockSets = array();
	private   $lineSets = array();
	private   $defineSets = array();
	private   $inBlockSet = NULL;
	//private	  $inListing = false;
	
	public $showBox = true;
	public $showFileName = true;
	public $showErrors = true;
	public $tabsToSpaces = "   ";
	public $fileNameSkip = NULL;

	public $showLineNumbers = true;
	public $showTrueLineNumbers = false;

	public $marker = NULL;

  public $base_dir = '';

	private $spanCont;
	private $cssClass;

	function __construct($cssClass) { //, $start = array(), $end = array(), $inclusive = true) {
		$this->cssClass = $cssClass;
		//parent::__construct($start, $end, $cssClass, $inclusive);
	}

	function registerKeywordSet($keywordSet) {
		array_push($this->keywordSets, $keywordSet);	
	}

	function registerBlockSet($blockSet) {
		array_push($this->blockSets, $blockSet);	
	}

	function registerLineSet($lineSet) {
		array_push($this->lineSets, $lineSet);
	}
	
	function registerDefineSet($defineSet) {
		array_push($this->defineSets, $defineSet);
		array_push($this->keywordSets, $defineSet);
	}

	function showFile($fileName) {
		$codeBlocks = array();

		for($i=1;$i<func_num_args();$i++) {
			$arg = func_get_arg($i);
			
			if(is_array($arg) && count($arg)>=3 ) {
				$className = $arg[0];
				for($j=1;$j<count($arg)-1;$j+=2) {
					$startLine = abs($arg[$j]);
					$endLine = abs($arg[$j+1]);
					if($startLine > $endLine) {
						continue;
					}
					switch ($className) {
						case "show";
							array_push($codeBlocks, new ShowBlock($startLine,$endLine));
							break;
							
						case "hide";
							array_push($codeBlocks,new HideBlock($startLine,$endLine));
							break;

						default:
							array_push($codeBlocks,new CodeBlock($startLine,$endLine,$className));
					}				
				
				}
			}		
		}
	
		$showLines = true;
		$oldCode = false;
		$nextBlockIndx = 0;
		$numBlocks = count($codeBlocks);
		if(!empty($codeBlocks)) {
			usort($codeBlocks, "codeBlock_compare");
			foreach($codeBlocks as $c) {
				if(is_bool($c->opt)) {
					$showLines = !$c->opt;
					break;
				}
			}

			foreach($codeBlocks as $c) {
				if(!is_bool($c->opt)) {
					if(strcmp($c->opt,'new')==0) {
						$oldCode = true;
						break;
					} else if(strcmp($c->opt,'old')==0) {
						$oldCode = false;
						break;					
					}
				}
			}

		}

		if( is_null($this->fileNameSkip) ) {
			$fileNameDisplay = $fileName;
		} else {
			$ext = pathinfo($fileName, PATHINFO_EXTENSION);
			$fileNameDisplay = strtok(basename($fileName), $this->fileNameSkip . ".") . "." . $ext;
		}

		if($this->showBox) {
      echo "<div class=\"codeblock\">\n";
      $fileUrl = $_SERVER['REQUEST_URI'];
      $fileUrl = substr($fileUrl, 0, strrpos( $fileUrl, '/'));
      $fileUrl = $fileUrl . "/" . $fileName;
      //dd($fileUrl);
      if($this->showFileName) {
				/*echo "<div class=\"codeblocktitle\"><a href=\"" . $fileUrl . "\">". basename($fileNameDisplay) ."</a></div>\n";*/
                                echo "<div class=\"codeblocktitle\">"
      . basename($fileNameDisplay) . "</div>\n";
			}
		}

		/* Parse the file and highlight the syntax */	
		$parsedLines = $this->lstFile($fileName);	
	
		$lineNumber = 1;
		$lineNumberRel = 1;
		$lineDigits = ceil(log10(count($parsedLines)+1));
		
		if($this->showLineNumbers) {
			$txtNumbers = "<pre class=\"linenumbers unselectable\" style=\"float: left;\">";
		} else {
			$txtNumbers = '';
		}
		$txtCode = "<pre class=\"code\">\n";
	
		$spans = new SplObjectStorage;
		foreach($parsedLines as $line) {
			$this->spanCount = 0;
			for($i=$nextBlockIndx;$i<$numBlocks;$i++) {
				$codeBlock = $codeBlocks[$i];
				if($codeBlock->startLine > $lineNumber) {
					break;				
				} else if($codeBlock->startLine == $lineNumber) {
					if(is_bool($codeBlock->opt)) {
						$showLines = $codeBlock->opt;
					} else if(strcmp($codeBlock->opt,'new')==0) {
						$oldCode = false;
					} else if(strcmp($codeBlock->opt,'old')==0) {
						$oldCode = true;
					} else {
						$spans->attach($codeBlock);
					}
				}	
			}
			
			if($showLines) {
				if ($oldCode) {
					$txtCode .= '<span
				class="codeold unselectable">';				
					$this->spanCount++;
				}
				foreach($spans as $x) {
					$txtCode .= '<span class="' . $x->opt . '">';
					$this->spanCount++;
				}
				
				if($this->showLineNumbers) {
					if($this->showTrueLineNumbers) {
						$showDigit = $lineNumber;
					} else {
						$showDigit = $lineNumberRel;
					}

					$digits = ceil(log10($showDigit+1));
					
					if ($oldCode) {
						$txtNumbers .= '<span
						class="codeold unselectable">';				
					}

					//$txtCode .= '<span class="unselectable">';
					//$this->spanCount++;
					for($i = $digits; $i<$lineDigits; $i++) {
						$txtNumbers .= "0";
					}
					$txtNumbers .= "$showDigit:\n";
					if ($oldCode) {
						$txtNumbers .= '</span>';				
					}
					//echo '</span>';
					//$this->spanCount--;
				}
				$txtCode .= $line;
				for($i=0;$i<$this->spanCount;$i++) {
					$txtCode .= "</span>";				
				}
				$txtCode .= "\n";
				$lineNumberRel++;
			}
			
			for($i=$nextBlockIndx;$i<$numBlocks;$i++) {
				$codeBlock = $codeBlocks[$i];
				if($codeBlock->startLine > $lineNumber) {
					break;
				} else if($codeBlock->endLine == $lineNumber) {
					if(is_bool($codeBlock->opt)) {
						$showLines = !$codeBlock->opt;
					} else if(strcmp($codeBlock->opt,'new')==0) {
						$oldCode = true;									
					} else if(strcmp($codeBlock->opt,'old')==0) {
						$oldCode = false;
					} else {
						$spans->detach($codeBlock);			
					}
				}
			}			
			
			$lineNumber++;
		}
		
		if($this->showLineNumbers) {
			$txtNumbers .= "</pre>";
		}
		$txtCode .= "</pre>\n";	
		echo $txtNumbers;
		echo $txtCode;
		if($this->showBox) {
			echo "</div>";
		}
		echo "\n";
	}
	
	function lstFile($fileName) {

		$fid = fopen($this->base_dir . $fileName, "r");

		/*if($startLine == $endLine) {
			echo "<tt>";
    	} else {
			echo "<pre>";
		}*/
		
		if (!$fid) {
			if($this->showErrors) {
				echo "Failed to open '$fileName'!";
			}
			return '';
		}
		
		$this->inBlockSet = NULL;		
		$parsedLines = array();

	   	while (($line = fgets($fid)) !== false) {
			// End of line closing block markers must be detected here
			// because parsed string is tripped of end-line characters
			/*if(!is_null($this->inBlockSet)) {
				$insideParser = NULL;
				for($i=0;$i<count($this->inBlockSet->internalParser);$i++) {
					if(empty($this->inBlockSet->internalParserBlock[$i])) {
						$insideParser = $this->inBlockSet->internalParser[$i];
						break;
					}					
				}
				if( empty($insideParser) ) {
					if($this->inBlockSet->checkEnd('\n')) {
						$this->inBlockSet = NULL;					
					}
				}
			}*/
			array_push($parsedLines,$this->parseString($line)[0]);
		}
		fclose($fid);
		return $parsedLines;
	}
	
	function showString($str, $dirName = NULL, $extName = NULL) {
		if( !is_null($dirName) ) {
		
			$files = scandir($dirName, 1);
	
			foreach ($files as $f) {
				if( !is_null($extName) ) {
					$ext = pathinfo($f, PATHINFO_EXTENSION);
					if($ext === $extName) {
						continue;
					}
				}
				$this->lstFile($dirName . "/" . $f);
			}
		}
		echo "<em>" . $this->parseString($str)[0] . "</em>";
	}
	
	private function parseString($line, $exitTest = NULL, $inmarked = false) {
	
		// Check if this is inner parsing, if yes, no need to
		// preprocess the $line
		if(is_null($exitTest)) {
			$line=str_replace("\t", $this->tabsToSpaces, $line);
			$line=str_replace("<", " <&lt;<", $line);
			$line=str_replace(">", " >&gt;>", $line);
			$line=str_replace(array("\n","\r"),'', $line);
			$line .= ' ';
		}

		// Check if need to span the listing - generally this is done
		// per each line, however, when inner parsing with inclusive markers
		// then the span has been created outside the function call
		if(!$inmarked) {
			$txt = '<span class="' . $this->cssClass . '">';
			$this->spanCount = 1;
		} else {
			$txt = '';
			$this->spanCount = 0;
		}

		// Check if in a block set - if block set has been started on a previous line
		// and not finished, next line should continue in the same block set		
		if(is_null($this->inBlockSet)) {
			$inBlockSetStart = false;
		} else {
			// If starting the line while inside a block set, check if that block
			// set has an inner parser with empty markers.  If yes, then will
			// parse with inner parser
			$inBlockSetStart = true;
			$insideParser = NULL;
			for($i=0;$i<count($this->inBlockSet->internalParser);$i++) {
				if(empty($this->inBlockSet->internalParserBlock[$i])) {
					$insideParser = $this->inBlockSet->internalParser[$i];
					break;
				}					
			}
			if(!empty($insideParser)) {
				// If inner parser is a string, create a new listing object
				if(is_string($insideParser)) {
					$insideParser = new $insideParser;
				}
				// Parse with inner parser
				$innerParsedTxt = $insideParser->parseString($line,$this->inBlockSet);
				$txt .= $innerParsedTxt[0];
				$line = $innerParsedTxt[1];	
				if(!empty($line)) {
					$txt = '<span class="' . $this->cssClass . '">';
					$this->spanCount++;
				}				
			}						
			$txt .= '<span class="' . $this->inBlockSet->cssClass . '">';
			$this->spanCount++;
		}
	
		// The number of characters in the $line is kept in a variable - it has to
		// be updated every time lenght of $line chagnes, but it will speed up a lot
		// of comparisions, because we won't have to call strlen() on the same
		// string over and over again
		$N = strlen($line);
		$keywordCheck = true;
		$nextDefine = NULL;
		$createDefinition = false;
		// Parse the entire line - parsing removes processed characters, and so
		// parsing continues until $line is empty
		while(!empty($line)) {
			// If this is inner parsing, check for exit condition - that condition
			if(!is_null($exitTest)) {
				if( $N >= $exitTest->M ) {
					if($exitTest->checkEnd($line)) {
						for($i=0;$i<$this->spanCount;$i++) {
							$txt .= '</span>';
						}
						$txt=str_replace(" <&lt;<", "&lt;", $txt);
						$txt=str_replace(" >&gt;>", "&gt;", $txt);
						return array($txt,$line);
					}
				
				}							
			}
			
			// Check if inside a block set
			if(!is_null($this->inBlockSet)) {
				$blockSet = $this->inBlockSet;

				// If inisde a block set, check for inner parser condition
				for($i=0;$i<count($blockSet->internalParser);$i++) {
					if(!empty($blockSet->internalParserBlock[$i])) {
						if( $N < $blockSet->internalParserBlock[$i]->N ) {
							continue;
						}
						if($blockSet->internalParserBlock[$i]->checkStart($line)) {					
							$insideParser = $blockSet->internalParser[$i];
							
							for($j=0;$j<$this->spanCount;$j++) {
								$txt .= "</span>";
							}
							
							if($blockSet->internalParserBlock[$i]->inclusiveStart) {
								$txt .= '<span class="' . $blockSet->internalParserBlock[$i]->cssClass . '">';
								$inParserMark = true;
							} else {
								$inParserMark = false;
							}							
			
							if( is_null($blockSet->internalParserBlock[$i]->replaceStart) ) {
								$txt .= substr($line,0,$blockSet->internalParserBlock[$i]->N);
							} else {
								$txt .= $blockSet->internalParserBlock[$i]->replaceStart;
							}
							$line = substr($line, $blockSet->internalParserBlock[$i]->N);			
							$N -= $blockSet->internalParserBlock[$i]->N;
						
							if(is_string($insideParser)) {
								$insideParser = new $insideParser;
							}
						
							$innerParsedTxt = $insideParser->parseString($line,$blockSet->internalParserBlock[$i],$inParserMark);
							$txt .= $innerParsedTxt[0];
							$line = $innerParsedTxt[1];

							if( is_null($blockSet->internalParserBlock[$i]->replaceStart) ) {
								$txt .= substr($line,0,$blockSet->internalParserBlock[$i]->M);
							} else {
								$txt .= $blockSet->internalParserBlock[$i]->replaceEnd;
							}
							$line = substr($line, $blockSet->internalParserBlock[$i]->M);			
							
							if($inParserMark) {
								$txt .= "</span>";
							}
							
							
							if(empty($line)) {
								$this->spanCount = 0;
							} else {
				    			$txt .= '<span class="' . $this->cssClass . '">';
				    			$txt .= '<span class="' . $blockSet->cssClass . '">';
								$this->spanCount = 2;
							}
							$N = strlen($line);		
							break;
						}
					}					
				}

				// Check for block set end marker to exit block set
				if( $N >= $blockSet->M ) {
					if($blockSet->checkEnd($line)) {
						if($blockSet->inclusiveEnd) {
							$txt .= substr($line,0,$blockSet->M) . '</span>';
							$this->spanCount--;
						} else {
							$txt .= '</span>' . substr($line,0,$blockSet->M);						
							$this->spanCount--;
						}
						$this->inBlockSet = NULL;
						$line = substr($line, $blockSet->M);			
						$N -= $blockSet->M;
						$keywordCheck = true;
						continue;				
					}
				}
				if( !(strlen($line) == 1 && $line == " ") ) {
					$txt .= $line[0];
				}
				$line = substr($line, 1);			
				$N--;	
				continue;
			} else {
				// If not inside block set, check start marker on all block sets
				// for start marker match
				for($i=0; $i<count($this->blockSets); $i++) {	
					$blockSet = $this->blockSets[$i];
					if( $N < $blockSet->N ) {
						continue;
					}
					if($blockSet->keywordCheck || $keywordCheck) {
						if($blockSet->checkStart($line)) {					
							$this->inBlockSet = $blockSet;
							if($blockSet->inclusiveStart) {
								$txt .= '<span class="' . $blockSet->cssClass . '">';
								if( is_null($blockSet->replaceStart) ) {
									$txt .= substr($line,0,$blockSet->N);
								} else {
									$txt .= $blockSet->replaceStart;								
								}
							} else {
								$txt .= substr($line,0,$blockSet->N) . '<span class="' . $blockSet->cssClass . '">';
							}							
							$this->spanCount++;
							$line = substr($line, $blockSet->N);			
							$N -= $blockSet->N;
							break;
						}
					}
				}
				if(!is_null($this->inBlockSet)) {
					$insideParser = NULL;
					for($i=0;$i<count($this->inBlockSet->internalParser);$i++) {
						if(empty($this->inBlockSet->internalParserBlock[$i])) {
							$insideParser = $this->inBlockSet->internalParser[$i];
							break;
						}					
					}
					if(!empty($insideParser)) {
						if(is_string($insideParser)) {
							$insideParser = new $insideParser;
						}
					
						$innerParsedTxt = $insideParser->parseString($line,$this->inBlockSet);
						for($i=0;$i<$this->spanCount;$i++) {
							$txt .= "</span>";
						}
						$txt .= $innerParsedTxt[0];
						$line = $innerParsedTxt[1];
						if(empty($line)) {
							$this->spanCount = 0;
						} else {
					    	$txt .= '<span class="' . $blockSet->cssClass . '">';
							$this->spanCount = 1;
						}
						$N = strlen($line);		
					}
					continue;
				}
			}


			foreach($this->lineSets as $lineSet) {
				if( $N < $lineSet->N) {
					continue;
				}
				if( strcmp($lineSet->start,substr($line,0,$lineSet->N))===0) {
					if( substr($line, -1) == " " ) {
						$line = substr($line, 0, -1);					
					}
				
					$txt .= '<span class="' . $lineSet->cssClass . '">' . $line . '</span>';
					for($i=0;$i<$this->spanCount;$i++) {
						$txt .= "</span>";
					}
					$txt=str_replace(" <&lt;<", "&lt;", $txt);
					$txt=str_replace(" >&gt;>", "&gt;", $txt);
					return array($txt,'');
				}
			}
			
			$keywordFound = false;
			if(is_null($this->inBlockSet) && $keywordCheck) {
				$justFoundDefineKeyword = false;
				if(is_null($nextDefine)) {
					foreach($this->defineSets as $defineSet) {
						foreach($defineSet->predefKeywords as $keyword) {
							$M = strlen($keyword);
							if( $N < $M) {
								continue;
							}
							if( $N==$M || !ctype_alnum($line[$M])) {
								if( strcmp($keyword,substr($line,0,$M))===0) {
									$nextDefine = $defineSet;
									$nextDefine->token = '';
									$justFoundDefineKeyword = true;
									break;
								}
							}
						}
						if(!is_null($nextDefine)) {
							break;
						}
					}
				}


				foreach($this->keywordSets as $keywordSet) {
					foreach($keywordSet->keywords as $keyword) {
						$M = strlen($keyword);
						if( $N < $M) {
							continue;
						}
						if( $N==$M || !ctype_alnum($line[$M])) {
							if( strcmp($keyword,substr($line,0,$M))===0) {
								if($keywordSet->mustFollow != "") {
									if($keywordSet->mustFollow != $line[$M]) {
										break;
									} elseif ($keywordSet->mustFollow == " ") {
										$ltemp = substr($line, $M);
										if(strlen($ltemp) >= strlen(" <&lt;<")) {
											$ltemp = substr($ltemp, 0, strlen(" <&lt;<"));
											if($ltemp == " <&lt;<" || $ltemp == " >&gt;>") {
												break;
											}
										} 	
									}
								} 
								$txt .= '<span class="' . $keywordSet->cssClass . '">' . $keyword . '</span>';
								$line = substr($line, $M);	
								$N -= $M;
								break;
							}
						}
					}
					if($keywordFound) {
						// Check if keyword is not one of the define keywords
						if(!$justFoundDefineKeyword) {
							$nextDefine = NULL;
						}
						break;
					}
				}

				/*if(!$keywordFound) {
					foreach($this->defineSets as $keywordSet) {
						foreach($keywordSet->keywords as $keyword) {
							$M = strlen($keyword);
							if( $N < $M) {
								continue;
							}
							if( $N==$M || !ctype_alnum($line[$M])) {
								if( strcmp($keyword,substr($line,0,$M))===0) {
									$txt .= '<span class="' . $keywordSet->cssClass . '">' . $keyword . '</span>';
									$line = substr($line, $M);	
									$N -= $M;
									$keywordFound = true;
									break;
								}
							}
						}
						if($keywordFound) {
							break;
						}
					}				
				}*/
				
			}
			if(!$keywordFound) {
				if( !(strlen($line) == 1 && $line == " ") ) {
					$txt .= $line[0];
				}
				if(ctype_alnum($line[0])) {
					$keywordCheck = false;
					if(!is_null($nextDefine)) {
						$nextDefine->token .= $line[0];
						$createDefinition = true;
					}
				} else {
					$keywordCheck = true;
					if(!is_null($nextDefine) && $createDefinition) {
						if($nextDefine->retro) {
							$follower = substr($txt,-1);
							$txt = substr($txt, 0, -strlen($nextDefine->token)-1);
							$txt .= '<span class="' . $nextDefine->cssClass . '">' . $nextDefine->token . '</span>' . $follower;
						}
						$nextDefine->append($nextDefine->token);
						$nextDefine = NULL;
						$createDefinition = false;
					}
				}
				$line = substr($line, 1);			
				$N--;				
			}
		}
		//$txt .= '</span>';		
	
		for($i=0;$i<$this->spanCount;$i++) {
			$txt .= '</span>';
		}
	
		$txt=str_replace(" <&lt;<", "&lt;", $txt);
		$txt=str_replace(" >&gt;>", "&gt;", $txt);
		return array($txt,'');
	}	
	
}


?>

