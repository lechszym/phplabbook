<?php

require_once('labbook.php');

class ListingUScript extends Listing {
	
	function __construct() {

		/* SWIFT keywords - just add any missing keywords to this array */
		$keywords = new KeywordSet("codekeyword","#bc1aaa");
		$keywords->append(array("function", "new", "if", "else", "static", "this", "private", "as", "in", "import", "public"));
		$this->registerKeywordSet($keywords);
		
		
		/* Foundation classes (have different colour than keywords) */
		$classes = new KeywordSet("codeclass","#7233aa");
		$classes->append(array("var", "float", "KeyCode", "Time", "Vector3", "Mathf", "Input", "null", "GUI", "Color", "Rect", "TextAnchor", "GUISkin", "FontStyle", "true", "false", "Camera", "GameObject", "boolean"));
		$this->registerKeywordSet($classes);

		$commentsBlock = new BlockSet("/*","*/","uscriptcomment");
		$this->registerBlockSet($commentsBlock);
		
		$commentsLine = new LineSet("//", "uscriptcomment");
		$this->registerLineSet($commentsLine);

		$stringBlock = new BlockSet("\"","\"", "uscriptstring");
		$this->registerBlockSet($stringBlock);	

		$hashBlock = new LineSet("#", "uscripthash");
		$this->registerLineSet($hashBlock);	
		
		$this->registerBlockSet(new NumberSet("uscriptnumber"));
		
	}


}


?>

