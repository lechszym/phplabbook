<?php

require_once('labbook.php');

class ListingPHP extends Listing {
	
	function __construct() {
		parent::__construct("codephp", "&lt;?php", "?&gt;");

		$this->htmlEscapes = true;

		$keywords = new KeywordSet("phpkeyword");
		$keywords->append(array("require", "function",
				  	"class", "extends", "return","new",
					"break","return","continue",
					"public","private","protected",
					"if", "else", "switch", "case", "default",
					"for", "do", "while"));
		$this->registerKeywordSet($keywords);

		$commentsBlock = new BlockSet("/*","*/","phpcomment");
		$this->registerBlockSet($commentsBlock);
		
		$commentsLine1 = new LineSet("//", "phpcomment");
		$this->registerLineSet($commentsLine1);

		$commentsLine2 = new LineSet("#", "phpcomment");
		$this->registerLineSet($commentsLine2);

		$stringBlock1 = new BlockSet("\"","\"", "phpstring");
		$this->registerBlockSet($stringBlock1);	
		
		$stringBlock2 = new BlockSet("'","'", "phpstring");
		$this->registerBlockSet($stringBlock2);	
		
		$var = new BlockSet("$",new nonAlphaNumericMarker('_'),"phpvar",true,false);
		$this->registerBlockset($var);
		
	}


}


?>

