<?php

require_once('labbook.php');

class ListingSwiftString extends Listing {
	function __construct() {
		$this->cssClass = "swiftstring";
		$varBlock = new BlockSet('\(', new BracketMarker(), "codeswift");
		$varBlock->parser = "ListingSwift";
		$this->registerBlockSet($varBlock);	
	}

}

class ListingSwift extends Listing {
	
	function __construct() {

		parent::__construct("swiftcode");

		/* SWIFT keywords - just add any missing keywords to this array */
		$keywords = new KeywordSet("swiftkeyword");
		$keywords->append(array("import", "weak",
				  	"static", "final", "convenience",
					"class", "func", "protocol", "typealias", "extension",
					"init", "deinit", "subscript", "deinit","get", "set", 
					"self", "super",
					"private", "public", "internal",
					"var", "let", "as", "in", "where",
					"break","return","continue",
					"if", "else", "switch", "case", "default",
					"for", "repeat", "while",
					"false", "true", "override", "internal", 
					"@IBOutlet", "@IBAction", "@NSApplicationMain"));
		$this->registerKeywordSet($keywords);
		
		
		/* Foundation classes (have different colour than keywords) - add any missing Foundation classes here */
		$classes = new KeywordSet("swiftclass");
		$classes->append(array("Int", "String", "Float", "Double", "Character", "Bool", 
						"Any","AnyObject","Self", "CustomStringConvertible",
						"NSString","NSApplicationDelegate", "NSWindow", "NSTextField",
						"NSNotification", "NSButton"));
		$this->registerKeywordSet($classes);

		/* Global functions (have different colour yet) - add any missing Foundation functions here */
		$functions = new KeywordSet("swiftfunction");
		$functions->append(array("println", "print"));
		$this->registerKeywordSet($functions);
			
			
		$commentsBlock = new BlockSet("/*","*/","swiftcomment");
		$this->registerBlockSet($commentsBlock);
		
		$commentsLine = new LineSet("//", "swiftcomment");
		$this->registerLineSet($commentsLine);

		$stringBlock = new BlockSet("\"","\"", "swiftstring");
		$stringInterpolationBlock = new BlockSet('\(', new BracketMarker(), "codeswift", true, NULL, "\<span class=\"swiftstring\">(</span>", "<span class=\"swiftstring\">)</span>");
		$stringBlock->registerInternalParser("ListingSwift", $stringInterpolationBlock);
		$this->registerBlockSet($stringBlock);	
		
		$this->registerBlockSet(new NumberSet("swiftnumber"));
		
		$defineSet = new DefineSet("swiftdefined", array("class", "protocol", "typealias"));
		
		$this->registerDefineSet($defineSet);
		
	}


}


?>

